# SoDApink #

This MaxMSP project is an application of [Brian Ellis'](http://www.brianellissound.com/) 
great [sounds.pink](https://sounds.pink/us.html) website 
for combining multiple stages (users) in the same session.

### What does the patch do? ###

Slight variations are done to Ellis'original MaxPatch, in order to be able to receive data from multiple stages.
At this point it seems to be capable of running several instances when quering the server one after another. 

### Problem ###

Either MaxMSP or the server side take too long to use multiple stages at once. 
It can still gather useful data, but not smooth enough to be used as a performance tool.

### Credits ###

[Sounds.pink](https://sounds.pink/us.html) and the original maxpatches are the brainchild 
of [Brian Ellis'](http://www.brianellissound.com/).