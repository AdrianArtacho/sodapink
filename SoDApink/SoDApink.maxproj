{
	"name" : "SoDApink",
	"version" : 1,
	"creationdate" : 3720272047,
	"modificationdate" : 3721131057,
	"viewrect" : [ 25.0, 104.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"SoDApink.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"soundsPink.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"PinkAverage.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"DataGraph.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"smooth-line.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"PanicKey.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"ParamRange.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"FilterOutCharacter.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"soundsPinkString.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"soundsPink.maxhelp" : 			{
				"kind" : "helpfile",
				"local" : 1
			}
,
			"soundsPinkEmotionSorter.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"flow_field.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"PinkIndex.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"media" : 		{
			"SoundsPink.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}

		}
,
		"code" : 		{
			"soundsPinkScript.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"soundsPinkRawOSC.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"soundsPinkDeviceScale.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
